package com.example.game_shop_feldkircher

import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.SearchView
import androidx.annotation.RequiresApi
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class PrestityActvity : AppCompatActivity() {

    val database = Firebase.database("https://gameshop-1614a-default-rtdb.europe-west1.firebasedatabase.app/").getReference("Prestiti")

    lateinit var listView: ListView
    var adapter: ListaPrestitiAdapter? = null
    var arrayList : ArrayList<Prestito> = ArrayList()
    var arrayListFiltered: ArrayList<Prestito> = ArrayList()
    var childListener : ChildEventListener = getChildListener()
    var date: Boolean = true
    var testRicerca: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prestiti)

        listView = findViewById(R.id.prestiti)

        adapter = ListaPrestitiAdapter(this, arrayList)
        listView.adapter = adapter

        var searchView: SearchView = findViewById(R.id.ricerca)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                arrayListFiltered.removeAll(arrayListFiltered)
                testRicerca = query
                if(query == ""){
                    adapter = ListaPrestitiAdapter(this@PrestityActvity, arrayList)
                    listView.adapter = adapter
                    return false
                }

                arrayList.forEach {
                    if(query in it.id)
                        arrayListFiltered.add(it)
                }
                adapter = ListaPrestitiAdapter(this@PrestityActvity, arrayListFiltered)
                listView.adapter = adapter
                return false
            }
            override fun onQueryTextChange(newText: String): Boolean {
                arrayListFiltered.removeAll(arrayListFiltered)
                testRicerca = newText
                if(newText == ""){
                    adapter = ListaPrestitiAdapter(this@PrestityActvity, arrayList)
                    listView.adapter = adapter
                    return false
                }
                arrayList.forEach {
                    if(newText in it.id)
                        arrayListFiltered.add(it)
                }
                adapter = ListaPrestitiAdapter(this@PrestityActvity, arrayListFiltered)
                listView.adapter = adapter
                return false
            }
        })
    }

    fun tornaIndietro(view: View) {
        val intent = Intent(this@PrestityActvity, HomeAdminActivity::class.java)
        startActivity(intent)
    }

    fun sortByDate(view: View){
        arrayListFiltered.removeAll(arrayListFiltered)
        var sortedList: List<Prestito> = arrayList.sortedWith(compareBy({ it.dataInizio.toInt() }))
        if(date) {
            sortedList.forEach {
                if(testRicerca in it.id)
                    arrayListFiltered.add(it)
            }
            date = false
        }else{
            sortedList.reversed().forEach {
                if(testRicerca in it.id)
                    arrayListFiltered.add(it)
            }
            date = true
        }

        adapter = ListaPrestitiAdapter(this@PrestityActvity, arrayListFiltered)
        listView.adapter = adapter
        return
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun visualizzaScaduti(view: View){
        val currentDateTime = LocalDateTime.now()
        val current = currentDateTime.format(DateTimeFormatter.BASIC_ISO_DATE)
        arrayListFiltered.removeAll(arrayListFiltered)
        arrayList.forEach{
            if(it.dataFine.toInt() < current.toInt())
                arrayListFiltered.add(it)
        }

        adapter = ListaPrestitiAdapter(this@PrestityActvity, arrayListFiltered)
        adapter?.notifyDataSetChanged()
        return
    }

    override fun onStart() {
        super.onStart()

        database!!.addChildEventListener(childListener)
    }

    internal fun getChildListener(): ChildEventListener {
        return object : ChildEventListener {

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {

                val nuovoPrestito = snapshot.getValue(Prestito::class.java)
                arrayList.add(nuovoPrestito!!)
                adapter?.notifyDataSetChanged()

            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {

                val nuovoPrestito = snapshot.getValue(Prestito::class.java)
                val userKey = snapshot.key
                arrayList.find { e -> e.toString() == userKey }?.set(nuovoPrestito!!)
                adapter?.notifyDataSetChanged()

            }

            override fun onChildRemoved(snapshot: DataSnapshot) {

                val userKey = snapshot.key
                val prestitoRimosso = arrayList.find { e -> e.toString() == userKey }
                arrayList.remove(prestitoRimosso)
                adapter?.notifyDataSetChanged()

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

                Log.d("PrestityActvity", "onChildMoved" + snapshot.key!!)

            }

            override fun onCancelled(error: DatabaseError) {

                Log.d("PrestityActvity", "onCancelled" + error.toException())

            }

        }
    }

    override fun onStop() {
        super.onStop()

        database!!.removeEventListener(childListener)
    }
}

class ListaPrestitiAdapter(private val context: Context, private val arrayList: ArrayList<Prestito>) : BaseAdapter() {
    private lateinit var gioco: TextView
    private lateinit var userName: TextView
    private lateinit var dataInizio: TextView
    private lateinit var dataFine: TextView
    private lateinit var termina: Button

    override fun getCount(): Int {
        return arrayList.size
    }
    override fun getItem(position: Int): Any {
        return position
    }
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var convertView = convertView
        convertView = LayoutInflater.from(context).inflate(R.layout.row_prestiti, parent, false)
        gioco = convertView.findViewById(R.id.gioco)
        userName = convertView.findViewById(R.id.userName)
        dataInizio = convertView.findViewById(R.id.dataInizio)
        dataFine = convertView.findViewById(R.id.dataFine)
        termina = convertView.findViewById(R.id.elimina)


        val formatter = DateTimeFormatter.ofPattern("yyyyMMdd")
        val dtf: LocalDate = LocalDate.parse(arrayList[position].dataFine, formatter)
        val dti: LocalDate = LocalDate.parse(arrayList[position].dataInizio, formatter)

        gioco.text = " " + arrayList[position].gioco
        userName.text = arrayList[position].userName
        dataInizio.text = dti.toString()
        dataFine.text = dtf.toString()



        termina.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                // Do some work here
                val database = Firebase.database("https://gameshop-1614a-default-rtdb.europe-west1.firebasedatabase.app/").getReference("Prestiti")
                database.child(arrayList[position].id).removeValue()

            }
        })

        return convertView
    }
}
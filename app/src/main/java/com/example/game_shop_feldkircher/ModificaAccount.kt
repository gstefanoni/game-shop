package com.example.game_shop_feldkircher

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_modifica_account.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.buttonAvventura
import kotlinx.android.synthetic.main.activity_register.buttonCasual
import kotlinx.android.synthetic.main.activity_register.buttonGDR
import kotlinx.android.synthetic.main.activity_register.buttonHorror
import kotlinx.android.synthetic.main.activity_register.buttonPicchiaduro
import kotlinx.android.synthetic.main.activity_register.buttonPlatform
import kotlinx.android.synthetic.main.activity_register.buttonSparatutto
import kotlinx.android.synthetic.main.activity_register.buttonSport
import kotlinx.android.synthetic.main.activity_register.buttonStrategico
import kotlinx.android.synthetic.main.row_prestiti.*

class ModificaAccount : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_modifica_account)

        val database = Firebase.database("https://gameshop-1614a-default-rtdb.europe-west1.firebasedatabase.app/").getReference("Utenti").child(globalUsername)

        var user: Utente?

        database.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dS: DataSnapshot) {

                user = dS.getValue(Utente::class.java)

                campoUsernameModifica.setText(user?.username)
                campoNomeModifica.setText(user?.nome)
                campoCognomeModifica.setText(user?.cognome)
                campoEmailModifica.setText(user?.email)
                campoTelefonoModifica.setText(user?.numeroTelefono)
                passwordModifica.setText(user?.password)
                confermaPasswordModifica.setText(user?.password)

                when (user?.categoria) {
                    "Avventura" -> buttonAvventura.isChecked = true
                    "Casual" -> buttonCasual.isChecked = true
                    "GDR" -> buttonGDR.isChecked = true
                    "Horror" -> buttonHorror.isChecked = true
                    "Picchiaduro" -> buttonPicchiaduro.isChecked = true
                    "Platform" -> buttonPlatform.isChecked = true
                    "Sparatutto" -> buttonSparatutto.isChecked = true
                    "Sport" -> buttonSport.isChecked = true
                    "Strategico" -> buttonStrategico.isChecked = true
                }
            }

            override fun onCancelled(dE: DatabaseError) {}
        })
    }

    private var selezionato = ""
    val database2 = Firebase.database("https://gameshop-1614a-default-rtdb.europe-west1.firebasedatabase.app/")

    fun indietroModifica(view: View) {
        val intent = Intent(this@ModificaAccount, HomeUtente::class.java)
        startActivity(intent)
    }

    fun modifica(view: View) {

        if(campoNomeModifica.text.toString() == "" || campoNomeModifica.text.toString() == "Nome"){
            Snackbar.make(findViewById(R.id.campoNomeModifica), "Campo Nome non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(campoCognomeModifica.text.toString() == "" || campoCognomeModifica.text.toString() == "Cognome"){
            Snackbar.make(findViewById(R.id.campoCognomeModifica), "Campo Cognome non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(campoEmailModifica.text.toString() == "" || campoEmailModifica.text.toString() == "Email"){
            Snackbar.make(findViewById(R.id.campoEmailModifica), "Campo Email non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(campoTelefonoModifica.text.toString() == "" || campoTelefonoModifica.text.toString() == "Numero di telefono"){
            Snackbar.make(findViewById(R.id.campoTelefonoModifica), "Campo telefono non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(passwordModifica.text.toString() == "" || passwordModifica.text.toString() == "Password"){
            Snackbar.make(findViewById(R.id.passwordRegistra), "Campo Password non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(confermaPasswordModifica.text.toString() == "" || confermaPasswordModifica.text.toString() == "Conferma Password"){
            Snackbar.make(findViewById(R.id.confermaPasswordModifica), "Campo Username non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(confermaPasswordModifica.text.toString() != passwordModifica.text.toString()) {
            Snackbar.make(findViewById(R.id.confermaPasswordModifica), "Le password non corrispondono", Snackbar.LENGTH_LONG).show()
            return
        }

        writeNewUser(campoUsernameModifica.text.toString(),campoNomeModifica.text.toString(),campoCognomeModifica.text.toString(),campoEmailModifica.text.toString(),campoTelefonoModifica.text.toString(),passwordModifica.text.toString(),selezionato)
        return Snackbar.make(findViewById(R.id.confermaPasswordModifica), "Modifica completata", Snackbar.LENGTH_LONG).show()
    }

    fun onRadioButtonClicked(view: View){
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                R.id.buttonAvventura ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Avventura"
                    }
                R.id.buttonCasual ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Casual"
                    }
                R.id.buttonGDR ->
                    if (checked) {
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "GDR"
                    }
                R.id.buttonHorror ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Horror"
                    }
                R.id.buttonPicchiaduro ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Picchiaduro"
                    }
                R.id.buttonPlatform ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Platform"
                    }
                R.id.buttonSparatutto ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Sparatutto"
                    }
                R.id.buttonSport ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Sport"
                    }
                R.id.buttonStrategico ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSport.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonAvventura.isChecked = false
                        selezionato = "Strategico"
                    }
            }
        }
    }
    
    fun writeNewUser(
        username: String,
        nome: String,
        cognome: String,
        email: String,
        numeroTelefono: String,
        password: String,
        radioButton: String
    ) {
        val nuovoUtente = Utente(username,nome,cognome,email,numeroTelefono,password,radioButton)
        database2.getReference("Utenti").child(globalUsername).setValue(nuovoUtente).addOnSuccessListener{Snackbar.make(findViewById(R.id.campoUsernameModifica), "Successo", Snackbar.LENGTH_LONG).show()  }.addOnFailureListener() { Snackbar.make(findViewById(R.id.campoUsernameModifica), "Fallimento", Snackbar.LENGTH_LONG).show()}
    }

}



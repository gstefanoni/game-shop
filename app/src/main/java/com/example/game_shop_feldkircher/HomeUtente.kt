package com.example.game_shop_feldkircher

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.main.activity_home.*
import java.util.*
import kotlin.collections.ArrayList


@SuppressLint("StaticFieldLeak")
@Suppress("DEPRECATION")

lateinit var listViewNovita: ListView
@SuppressLint("StaticFieldLeak")
lateinit var listViewConsigliati: ListView
@SuppressLint("StaticFieldLeak")
lateinit var listViewPrestiti: ListView



class HomeUtente : AppCompatActivity(), OnItemClickListener{

    var arrayListNovita: ArrayList<Gioco> = ArrayList()
    var arrayListConsigliati: ArrayList<Gioco> = ArrayList()
    var arrayListPrestiti: ArrayList<Gioco> = ArrayList()

    var arrayGiochi: ArrayList<Gioco> = ArrayList()
    var arrayPrestiti: ArrayList<Prestito> = ArrayList()

    val database = Firebase.database("https://gameshop-1614a-default-rtdb.europe-west1.firebasedatabase.app/")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_home)

        database.reference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {

                var snapGiochi = snapshot.child("Giochi")
                for (postSnapshot in snapGiochi.children) {
                    val gioco: Gioco? = postSnapshot.getValue(Gioco::class.java)
                    if (gioco != null) {
                        arrayGiochi.add(gioco)
                    }
                }

                var snapPrestiti = snapshot.child("Prestiti")
                for (postSnapshot in snapPrestiti.children) {
                    val prestito: Prestito? = postSnapshot.getValue(Prestito::class.java)
                    if (prestito != null) {
                        arrayPrestiti.add(prestito)
                    }
                }

                val recyclerViewConsigliati = findViewById<RecyclerView>(com.example.game_shop_feldkircher.R.id.recyclerViewConsigliati)
                createConsigliati()
                recyclerViewConsigliati.layoutManager = LinearLayoutManager(this@HomeUtente, LinearLayoutManager.HORIZONTAL, false)
                var giocoAdapterConsigliati = GiocoAdapter(arrayListConsigliati, this@HomeUtente)
                recyclerViewConsigliati.adapter = giocoAdapterConsigliati

                val recyclerViewNovita = findViewById<RecyclerView>(com.example.game_shop_feldkircher.R.id.recyclerViewNovita)
                createNovita()
                recyclerViewNovita.layoutManager = LinearLayoutManager(this@HomeUtente, LinearLayoutManager.HORIZONTAL, false)
                var giocoAdapterNovita = GiocoAdapter(arrayListNovita,this@HomeUtente)
                recyclerViewNovita.adapter = giocoAdapterNovita

                val recyclerViewPrestiti = findViewById<RecyclerView>(com.example.game_shop_feldkircher.R.id.recyclerViewPrestiti)
                createPrestiti()
                recyclerViewPrestiti.layoutManager = LinearLayoutManager(this@HomeUtente, LinearLayoutManager.HORIZONTAL, false)
                var giocoAdapterPrestiti = GiocoAdapter(arrayListPrestiti,this@HomeUtente)
                recyclerViewPrestiti.adapter = giocoAdapterPrestiti

                var searchView: SearchView = findViewById(R.id.ricercaHome)

                searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String): Boolean {
                        for(gioco in arrayGiochi) {
                            if (gioco.nome == query) {
                                nodePath=gioco.id
                                val intent = Intent(this@HomeUtente, SchedaGioco::class.java)
                                startActivity(intent)
                            }
                            else{
                                Snackbar.make(findViewById(R.id.ricercaHome), "Il gioco cercato non risiede nel database.", Snackbar.LENGTH_LONG).show()
                            }
                        }
                        return false
                    }


                    override fun onQueryTextChange(newText: String?): Boolean {
                        return false
                    }
                })

            }

            override fun onCancelled(databaseError: DatabaseError) {
                System.out.println("The prestiti read failed")
            }
        })

    }

    fun modAccount(view: View) {
        val intent = Intent(this@HomeUtente, ModificaAccount::class.java)
        startActivity(intent)
    }

    fun indietroLogin(view: View) {
        val intent = Intent(this@HomeUtente, MainActivity::class.java)
        startActivity(intent)
    }

    fun createConsigliati() {
        arrayGiochi.forEach {
            if (globalUser.categoria == it.categoria) {
                arrayListConsigliati.add(it)
                }
            }
        }

    fun createNovita() {
        var orderedArray: List<Gioco> = arrayGiochi.sortedWith(compareBy({ it.data.toInt() }))
        orderedArray.forEach {
            arrayListNovita.add(it)
            }
        }

    fun createPrestiti() {
            arrayPrestiti.forEach {
                var id = it.gioco
                if (globalUser.username == it.userName) {
                    for (gioco in arrayGiochi) {
                        if (gioco.id == id){
                            arrayListPrestiti.add(gioco)
                            }
                        }
                    }
                }
            }

    override fun onItemClick(position: Int, giochiList: List<Gioco>) {
        nodePath = giochiList[position].id
        val intent = Intent(this@HomeUtente, SchedaGioco::class.java)
        startActivity(intent)
    }


}



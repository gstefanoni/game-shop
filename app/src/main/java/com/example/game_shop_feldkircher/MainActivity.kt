package com.example.game_shop_feldkircher

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

var globalUsername = ""
var globalUser: Utente = Utente()
var nodePath = ""

@Suppress("UNUSED_PARAMETER", "DEPRECATION")
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_main)



        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("message")
        myRef.setValue("Hello, World!").addOnSuccessListener {
            Snackbar.make(findViewById(R.id.campoUsernameRegistrazione), "Successo", Snackbar.LENGTH_LONG).show()
        }
    }


        fun register(view: View) {
            val intent = Intent(this@MainActivity, RegisterActivity::class.java)
            startActivity(intent)
        }

        fun indietroLogin(view: View) {
            val intent = Intent(this@MainActivity, MainActivity::class.java)
            startActivity(intent)
        }

        fun loginAdmin(viev: View) {
            val intent = Intent(this@MainActivity, LoginAdmin::class.java)
            startActivity(intent)
        }

        fun indietroModifica(view: View) {
            val intent = Intent(this@MainActivity, HomeUtente::class.java)
            startActivity(intent)
        }

    fun login(view: View) {

        if(campoUsernameAccesso.text.toString() == "" || loginPasswordAccesso.text.toString() == "")
            Snackbar.make(findViewById(R.id.loginPasswordAccesso), "Compilare tutti i campi.", Snackbar.LENGTH_LONG).show()
        else{

            val database = Firebase.database("https://gameshop-1614a-default-rtdb.europe-west1.firebasedatabase.app/").getReference("Utenti").child(campoUsernameAccesso.text.toString())

            var user: Utente? = Utente()

            database.addListenerForSingleValueEvent(object : ValueEventListener{
                override fun onDataChange(dS: DataSnapshot) {

                    user = dS.getValue(Utente::class.java)

                    if( user?.username.toString() == campoUsernameAccesso.text.toString() && user?.password.toString() == loginPasswordAccesso.text.toString()){
                        globalUsername = campoUsernameAccesso.text.toString()
                        globalUser = user!!
                        val intent = Intent(this@MainActivity, HomeUtente::class.java)
                        startActivity(intent)
                    } else {
                        Snackbar.make(
                            findViewById(R.id.loginPasswordAccesso),
                            "Username o Password errati.",
                            Snackbar.LENGTH_LONG
                        ).show()
                    }
                }

                override fun onCancelled(dE: DatabaseError) {}
            })
        }}
}


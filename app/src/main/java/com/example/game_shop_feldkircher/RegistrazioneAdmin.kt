package com.example.game_shop_feldkircher

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register_admin.*

class RegistrazioneAdmin : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_register_admin)

    }

    val database = Firebase.database("https://gameshop-1614a-default-rtdb.europe-west1.firebasedatabase.app/")

    fun backRegisterAdmin(view: View) {
        val intent = Intent(this@RegistrazioneAdmin, LoginAdmin::class.java)
        startActivity(intent)
    }

    fun registraNuovoAdmin(view: View) {

        if(campoUsernameRegistrazioneAdmin.text.toString() == "" || campoUsernameRegistrazioneAdmin.text.toString() == "Username"){
            Snackbar.make(findViewById(R.id.campoUsernameRegistrazioneAdmin), "Campo Username non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(passwordRegistrazioneAdmin.text.toString() == "" || passwordRegistrazioneAdmin.text.toString() == "Password"){
            Snackbar.make(findViewById(R.id.passwordRegistrazioneAdmin), "Campo Password non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(confermaPasswordRegistrazioneAdmin.text.toString() == "" || confermaPasswordRegistrazioneAdmin.text.toString() == "Conferma Password"){
            Snackbar.make(findViewById(R.id.confermaPasswordRegistrazioneAdmin), "Campo Username non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(confermaPasswordRegistrazioneAdmin.text.toString() != passwordRegistrazioneAdmin.text.toString()) {
            Snackbar.make(findViewById(R.id.buttonRegistrazioneAdmin), "Le password non corrispondono", Snackbar.LENGTH_LONG).show()
            return
        }

        writeNewAdmin(campoUsernameRegistrazioneAdmin.text.toString(), passwordRegistrazioneAdmin.text.toString())

    }

    fun writeNewAdmin(username: String, password: String ){

        val admin = Admin(username,password)

        database.getReference("Admin").child(username).setValue(admin).addOnSuccessListener{Snackbar.make(findViewById(R.id.campoUsernameRegistrazioneAdmin), "Successo", Snackbar.LENGTH_LONG).show()  }.addOnFailureListener() { Snackbar.make(findViewById(R.id.campoUsernameRegistrazioneAdmin), "Fallimento", Snackbar.LENGTH_LONG).show()}
    }

}


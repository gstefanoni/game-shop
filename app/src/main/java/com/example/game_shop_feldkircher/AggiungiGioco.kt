package com.example.game_shop_feldkircher

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.InputFilter
import android.text.Spanned
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import kotlinx.android.synthetic.main.activity_register.*
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


// One Button
var BSelectImage: Button? = null

// One Preview Image
var IVPreviewImage: ImageView? = null

// constant to compare
// the activity result code
var SELECT_PICTURE = 200
var check = 0



class AggiungiGioco: AppCompatActivity() {

    val database = Firebase.database("https://gameshop-1614a-default-rtdb.europe-west1.firebasedatabase.app/")
    val storage = Firebase.storage("gs://gameshop-1614a.appspot.com")
    lateinit var imgPath: String
    private var selezionato = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.aggiungi_gioco)
        // register the UI widgets with their appropriate IDs
        BSelectImage = findViewById(R.id.selectImg)
        IVPreviewImage = findViewById(R.id.immagineGioco)
        var prezzo: TextView
        prezzo = findViewById(R.id.prezzo)
        prezzo.setFilters(arrayOf<InputFilter>(DecimalDigitsInputFilter(5,2)))

    }

    fun tornaIndietro(view: View) {
        val intent = Intent(this@AggiungiGioco, HomeAdminActivity::class.java)
        startActivity(intent)
    }

    fun selezionaImmagine(view: View) {
        val i = Intent()
        i.type = "image/*"
        i.action = Intent.ACTION_GET_CONTENT

        startActivityForResult(Intent.createChooser(i, "Select Picture"), SELECT_PICTURE)
        check = 1

    }

    var selectedPhotoUri : Uri? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            // compare the resultCode with the
            // SELECT_PICTURE constant
            if (requestCode == SELECT_PICTURE) {
                // Get the url of the image from data
                selectedPhotoUri= data?.data
                if (null != selectedPhotoUri) {
                    // update the preview image in the layout
                    IVPreviewImage!!.setImageURI(selectedPhotoUri)
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun registra(view: View){

        var nome: TextView
        nome = findViewById(R.id.nomeGioco)
        var prezzo: TextView
        prezzo = findViewById(R.id.prezzo)
        var descrizione: TextView
        descrizione = findViewById(R.id.descrizione)
        var img: ImageView
        img = findViewById(R.id.immagineGioco)
        var radioGroup: RadioGroup
        radioGroup = findViewById(R.id.radioGroup)
        var radioButton: RadioButton


        //Verifica campi
        if (TextUtils.isEmpty(nome.getText().toString()) || TextUtils.isEmpty(prezzo.getText().toString()) ||
                TextUtils.isEmpty(descrizione.getText().toString()) || check == 0 ){
            val t1 = Toast.makeText(this, "Tutti i campi sono obbligatori", Toast.LENGTH_SHORT)
            t1.show()
            return
        }

        if (radioGroup.getCheckedRadioButtonId() == -1)
        {
            // no radio buttons are checked
            val t2 = Toast.makeText(this, "Selezionare una categoria", Toast.LENGTH_SHORT)
            t2.show()
            //return
        }

        if (check == 0){
            // no radio buttons are checked
            val t4 = Toast.makeText(this, "Selezionare un'immagine'", Toast.LENGTH_SHORT)
            t4.show()
            return
        }

        uploadImgToFirebaseStorage()


        val currentDateTime = LocalDateTime.now()
        val current = currentDateTime.format(DateTimeFormatter.BASIC_ISO_DATE)
        writeNewGioco(nome.getText().toString(), prezzo.getText().toString(), selezionato, descrizione.getText().toString(), imgPath, current)

        nome.setText("")
        prezzo.setText("")
        descrizione.setText("")
        imgPath = ""
        img.setImageURI(null)
        check = 0
        return


    }

    fun onRadioButtonClicked(view: View){
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                R.id.buttonAvventura ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Avventura"
                    }
                R.id.buttonCasual ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Casual"
                    }
                R.id.buttonGDR ->
                    if (checked) {
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "GDR"
                    }
                R.id.buttonHorror ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Horror"
                    }
                R.id.buttonPicchiaduro ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Picchiaduro"
                    }
                R.id.buttonPlatform ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Platform"
                    }
                R.id.buttonSparatutto ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Sparatutto"
                    }
                R.id.buttonSport ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Sport"
                    }
                R.id.buttonStrategico ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSport.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonAvventura.isChecked = false
                        selezionato = "Strategico"
                    }
            }
        }
    }

    fun writeNewGioco(nome: String, prezzo: String, categoria: String, descrizione: String, imgPath: String, current: String) {


        val id = "${UUID.randomUUID()}"


        val nuovoGioco = Gioco(id, nome, prezzo, categoria, descrizione, imgPath, current)

        database.getReference("Giochi").child(id).setValue(nuovoGioco).addOnSuccessListener{ Snackbar.make(findViewById(R.id.nomeGioco), "Successo", Snackbar.LENGTH_LONG).show()  }.addOnFailureListener() { Snackbar.make(findViewById(R.id.nomeGioco), "Fallimento", Snackbar.LENGTH_LONG).show()}
    }

    fun uploadImgToFirebaseStorage(){
        if(selectedPhotoUri == null) return

        val fileName = UUID.randomUUID().toString()
        val ref = storage.getReference("images/$fileName")
        imgPath = "images/$fileName"
        ref.putFile(selectedPhotoUri!!)
                .addOnSuccessListener {
                    Log.d("AggiungiGioco", "Immagine caricata ${it.metadata?.path}")
                }
        ref.downloadUrl.addOnSuccessListener {
            Log.d("AggiungiGioco", "path: $it")
        }
    }
}


class DecimalDigitsInputFilter(digitsBeforeZero: Int, digitsAfterZero: Int) :
    InputFilter {
    var mPattern: Pattern
    override fun filter(
        source: CharSequence,
        start: Int,
        end: Int,
        dest: Spanned,
        dstart: Int,
        dend: Int
    ): CharSequence? {
        val matcher: Matcher = mPattern.matcher(dest)
        return if (!matcher.matches()) "" else null
    }

    init {
        mPattern =
            Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1) + "}+((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?")
    }
}
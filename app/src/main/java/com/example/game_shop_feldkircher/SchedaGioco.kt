package com.example.game_shop_feldkircher

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.ArrayList
import com.squareup.picasso.Picasso



class SchedaGioco: AppCompatActivity() {

    val database = Firebase.database("https://gameshop-1614a-default-rtdb.europe-west1.firebasedatabase.app/").getReference("Giochi")
    val database2 = Firebase.database("https://gameshop-1614a-default-rtdb.europe-west1.firebasedatabase.app/").getReference("Prestiti")
    val storage = Firebase.storage("gs://gameshop-1614a.appspot.com")

    lateinit var nome: TextView
    lateinit var img: ImageView
    lateinit var categoria: TextView
    lateinit var descrizione: TextView
    lateinit var noleggia: Button
    lateinit var rinnova: Button
    lateinit var termina: Button
    lateinit var dataFineView: TextView

    var trovato = false

    var giocoSelezionato : Gioco? = null
    var prestito : Prestito? = null
    var arrayList : ArrayList<Prestito> = ArrayList()
    var childListener : ChildEventListener = getChildListener()

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("SetTextI18n")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scheda_gioco)
        nome = findViewById(R.id.titolo)
        categoria = findViewById(R.id.categoria)
        descrizione = findViewById(R.id.descrizione)
        noleggia = findViewById(R.id.noleggia)
        rinnova = findViewById(R.id.rinnova)
        termina = findViewById(R.id.termina)
        dataFineView = findViewById(R.id.datafine)
        img = findViewById(R.id.immagineGioco)

        //Caricare dati gioco selezionato
        //Verificare se esiste già il prestito per Utente/Gioco
        caricaGioco(nodePath)

    }

    fun tornaIndietro(view: View){
        val intent = Intent(this@SchedaGioco, HomeUtente::class.java)
        startActivity(intent)
    }

    fun terminaPrestito(view: View){
        //Rimuovere prestito
        database2.child(prestito!!.id).removeValue().addOnSuccessListener{ Snackbar.make(findViewById(R.id.titolo), "Successo", Snackbar.LENGTH_LONG).show()  }.addOnFailureListener() { Snackbar.make(findViewById(R.id.titolo), "Fallimento", Snackbar.LENGTH_LONG).show()}
        val intent = Intent(this@SchedaGioco, SchedaGioco::class.java)
        startActivity(intent)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun noleggia(view: View){
        //Aggiungere prestito
        val id = "${UUID.randomUUID()}"
        val currentDateTime = LocalDateTime.now()
        val current = currentDateTime.format(DateTimeFormatter.BASIC_ISO_DATE)
        val finePrestito = LocalDateTime.now().plusDays(14).format(DateTimeFormatter.BASIC_ISO_DATE)
        val nPrestito = Prestito(id, nodePath, globalUsername, current, finePrestito)
        database2.child(id).setValue(nPrestito).addOnSuccessListener{ Snackbar.make(findViewById(R.id.titolo), "Successo", Snackbar.LENGTH_LONG).show()  }.addOnFailureListener() { Snackbar.make(findViewById(R.id.titolo), "Fallimento", Snackbar.LENGTH_LONG).show()}
        val intent = Intent(this@SchedaGioco, SchedaGioco::class.java)
        startActivity(intent)

    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun rinnova(view: View){
        //Modiifcare data fine prestito

        val formatter = DateTimeFormatter.ofPattern("yyyyMMdd")
        val dt: LocalDate = LocalDate.parse(prestito!!.dataFine, formatter)
        val nuovaFine = dt.plusDays(14).format(DateTimeFormatter.BASIC_ISO_DATE)
        val nPrestito = Prestito(prestito!!.id, prestito!!.gioco, prestito!!.userName, prestito!!.dataInizio, nuovaFine)
        database2.child(prestito!!.id).setValue(nPrestito)
        val intent = Intent(this@SchedaGioco, SchedaGioco::class.java)
        startActivity(intent)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun caricaGioco(nome: String) {
        var value : Gioco? = Gioco()
        setGioco(value)
        database.child(nome).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                value = snapshot.getValue(Gioco::class.java)
                Log.i("SchedaGioco", "interno: ${value.toString()}")
                setGioco(value)
            }

            override fun onCancelled(error: DatabaseError) {
                value = Gioco()
                setGioco(value)
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun setGioco(g: Gioco?){
        giocoSelezionato = g
        Log.i("SchedaGioco", "setGioco g: ${giocoSelezionato.toString()}")
        nome.setText(g!!.nome)
        categoria.setText(g!!.categoria)
        if(g!!.nome != ""){
            descrizione.setText(g!!.descrizione)
            noleggia.setText("Noleggia - €${g!!.prezzo}")
            storage.getReference(g!!.imgPath).downloadUrl.addOnSuccessListener {
                Picasso.get().load(it).into(img)
                Log.i("Scheda gioco", it.toString())
            }.addOnFailureListener {
                // Handle any errors
            }
        }else{
            descrizione.setText("Gioco non disponibile al noleggio")
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun verificaPrestito(gioco: String, utente: String, prestitoN: Prestito){

        if(prestitoN.userName == utente && prestitoN.gioco == gioco){
        //Visualizzare info e bottoni corretti
            rinnova.visibility = View.VISIBLE
            termina.visibility = View.VISIBLE
            noleggia.visibility = View.GONE
            val formatter = DateTimeFormatter.ofPattern("yyyyMMdd")
            val dt: LocalDate = LocalDate.parse(prestitoN.dataFine, formatter)
            dataFineView.text = "Data fine prestito: ${dt}, \nrinnovo prestito al prezzo di: €${giocoSelezionato!!.prezzo} \nN.B. Terminando il prestito non si otterà alcun rimborso"
            dataFineView.visibility = View.VISIBLE
            prestito = prestitoN
            trovato = true
        }
        if (giocoSelezionato!!.nome != "" && !trovato){
            rinnova.visibility = View.GONE
            termina.visibility = View.GONE
            noleggia.visibility = View.VISIBLE
            dataFineView.visibility = View.GONE
        }else if (!trovato){
            rinnova.visibility = View.GONE
            termina.visibility = View.VISIBLE
            noleggia.visibility = View.GONE
            dataFineView.text = "N.B. Terminando il prestito non si otterà alcun rimborso"
            dataFineView.visibility = View.VISIBLE
        }
    }


    override fun onStart() {
        super.onStart()

        database2.addChildEventListener(childListener)
    }

    internal fun getChildListener(): ChildEventListener {
        return object : ChildEventListener {

            @RequiresApi(Build.VERSION_CODES.O)
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {

                val nuovoPrestito = snapshot.getValue(Prestito::class.java)
                arrayList.add(nuovoPrestito!!)

                verificaPrestito(nodePath, globalUsername, nuovoPrestito)

            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {

                val nuovoPrestito = snapshot.getValue(Prestito::class.java)
                val userKey = snapshot.key
                arrayList.find { e -> e.toString() == userKey }?.set(nuovoPrestito!!)

            }

            override fun onChildRemoved(snapshot: DataSnapshot) {

                val userKey = snapshot.key
                val prestitoRimosso = arrayList.find { e -> e.toString() == userKey }
                arrayList.remove(prestitoRimosso)

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

                Log.d("PrestityActvity", "onChildMoved" + snapshot.key!!)

            }

            override fun onCancelled(error: DatabaseError) {

                Log.d("PrestityActvity", "onCancelled" + error.toException())

            }

        }
    }

    override fun onStop() {
        super.onStop()

        database2.removeEventListener(childListener)
    }
}
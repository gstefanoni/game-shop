package com.example.game_shop_feldkircher

class Gioco constructor(var id: String, var nome: String, var prezzo: String, var categoria: String, var descrizione: String, var imgPath: String, var data: String) {

    constructor() : this("","","", "", "", "", "00000000")

    override fun toString(): String {
        return id.toString()
    }

    fun set(nuovoGioco: Gioco) {
        this.id = nuovoGioco.id
        this.nome = nuovoGioco.nome
        this.prezzo = nuovoGioco.prezzo
        this.descrizione = nuovoGioco.descrizione
        this.imgPath = nuovoGioco.imgPath
        this.data = nuovoGioco.data
    }
}
package com.example.game_shop_feldkircher

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.WindowManager

class HomeAdminActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_admin)
    }

    fun aggiungiGioco(view: View){
        val intent = Intent(this@HomeAdminActivity, AggiungiGioco::class.java)
        startActivity(intent)
    }

    fun visualizziPrestiti(view: View){
        val intent = Intent(this@HomeAdminActivity, PrestityActvity::class.java)
        startActivity(intent)
    }

    fun rimuoviGioco(view: View){
        val intent = Intent(this@HomeAdminActivity, RimuoviGiocoActivity::class.java)
        startActivity(intent)
    }

    fun disconnetti(view: View){
        globalUsername = ""
        val intent = Intent(this@HomeAdminActivity, MainActivity::class.java)
        startActivity(intent)
    }
}
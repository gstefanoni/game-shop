package com.example.game_shop_feldkircher

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.ktx.*
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity(){

    val database = Firebase.database("https://gameshop-1614a-default-rtdb.europe-west1.firebasedatabase.app/")
    private var selezionato = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_register)

    }
    fun indietroRegister(view: View) {
        val intent = Intent(this@RegisterActivity, MainActivity::class.java)
        startActivity(intent)
    }

    fun writeNewUser(
        username: String,
        nome: String,
        cognome: String,
        email: String,
        numeroTelefono: String,
        password: String,
        onRadioButtonClicked: String
    ) {

        val nuovoUtente = Utente(username,nome,cognome,email,numeroTelefono,password,onRadioButtonClicked)
        val intent = Intent(this@RegisterActivity, MainActivity::class.java)

        database.getReference("Utenti").child(username).setValue(nuovoUtente).addOnSuccessListener{
            Snackbar.make(findViewById(R.id.campoUsernameRegistrazione), "Successo", Snackbar.LENGTH_LONG).show()
            startActivity(intent)
        }.addOnFailureListener() { Snackbar.make(findViewById(R.id.campoUsernameRegistrazione), "Fallimento", Snackbar.LENGTH_LONG).show()}
    }


    fun onRadioButtonClicked(view: View){
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                R.id.buttonAvventura ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Avventura"
                    }
                R.id.buttonCasual ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Casual"
                    }
                R.id.buttonGDR ->
                    if (checked) {
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "GDR"
                    }
                R.id.buttonHorror ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Horror"
                    }
                R.id.buttonPicchiaduro ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Picchiaduro"
                    }
                R.id.buttonPlatform ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Platform"
                    }
                R.id.buttonSparatutto ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSport.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Sparatutto"
                    }
                R.id.buttonSport ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonAvventura.isChecked = false
                        buttonStrategico.isChecked = false
                        selezionato = "Sport"
                    }
                R.id.buttonStrategico ->
                    if (checked) {
                        buttonGDR.isChecked = false
                        buttonCasual.isChecked = false
                        buttonHorror.isChecked = false
                        buttonPicchiaduro.isChecked = false
                        buttonPlatform.isChecked = false
                        buttonSport.isChecked = false
                        buttonSparatutto.isChecked = false
                        buttonAvventura.isChecked = false
                        selezionato = "Strategico"
                    }
            }
        }
    }

    fun register(view: View) {
        if(campoUsernameRegistrazione.text.toString() == "" || campoUsernameRegistrazione.text.toString() == "Username"){
            Snackbar.make(findViewById(R.id.campoUsernameRegistrazione), "Campo Username non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(campoNomeRegistrazione.text.toString() == "" || campoNomeRegistrazione.text.toString() == "Nome"){
            Snackbar.make(findViewById(R.id.campoNomeRegistrazione), "Campo Nome non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(campoCognomeRegistrazione.text.toString() == "" || campoCognomeRegistrazione.text.toString() == "Cognome"){
            Snackbar.make(findViewById(R.id.campoCognomeRegistrazione), "Campo Cognome non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(campoEmailRegistrazione.text.toString() == "" || campoEmailRegistrazione.text.toString() == "Email"){
            Snackbar.make(findViewById(R.id.campoEmailRegistrazione), "Campo Email non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(campoTelefonoRegistrazione.text.toString() == "" || campoTelefonoRegistrazione.text.toString() == "Numero di telefono"){
            Snackbar.make(findViewById(R.id.campoTelefonoRegistrazione), "Campo telefono non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(passwordRegistra.text.toString() == "" || passwordRegistra.text.toString() == "Password"){
            Snackbar.make(findViewById(R.id.passwordRegistra), "Campo Password non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(confermaPasswordRegistrazione.text.toString() == "" || confermaPasswordRegistrazione.text.toString() == "Conferma Password"){
            Snackbar.make(findViewById(R.id.confermaPasswordRegistrazione), "Campo Username non compilato", Snackbar.LENGTH_LONG).show()
            return
        }

        if(confermaPasswordRegistrazione.text.toString() != passwordRegistra.text.toString()) {
            Snackbar.make(findViewById(R.id.confermaPasswordRegistrazione), "Le password non corrispondono", Snackbar.LENGTH_LONG).show()
            return
        }

        writeNewUser(campoUsernameRegistrazione.text.toString(),campoNomeRegistrazione.text.toString(),campoCognomeRegistrazione.text.toString(),campoEmailRegistrazione.text.toString(),campoTelefonoRegistrazione.text.toString(),passwordRegistra.text.toString(),selezionato)
        return Snackbar.make(findViewById(R.id.confermaPasswordRegistrazione), "Registrazione completata", Snackbar.LENGTH_LONG).show()

    }
}

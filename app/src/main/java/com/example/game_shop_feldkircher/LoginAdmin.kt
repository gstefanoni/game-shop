package com.example.game_shop_feldkircher

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_accesso_admin.*
import kotlinx.android.synthetic.main.activity_main.*

class LoginAdmin : AppCompatActivity(){
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_accesso_admin)
    }

    fun indietroLogin(view: View) {
        val intent = Intent(this@LoginAdmin, MainActivity::class.java)
        startActivity(intent)
        }

    fun registerAdmin(view: View) {
        val intent = Intent(this@LoginAdmin, RegistrazioneAdmin::class.java)
        startActivity(intent)
    }

    fun logAdmin(view: View) {

        if (campoUsernameAdmin.text.toString() == "" || loginPasswordAdmin.text.toString() == "")
            Snackbar.make(
                findViewById(R.id.loginPasswordAdmin),
                "Compilare tutti i campi.",
                Snackbar.LENGTH_LONG
            ).show()
        else {
            val database =
                Firebase.database("https://gameshop-1614a-default-rtdb.europe-west1.firebasedatabase.app/")
                    .getReference("Admin").child(campoUsernameAdmin.text.toString())

            var user: Admin? = Admin()

            database.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dS: DataSnapshot) {

                    user = dS.getValue(Admin::class.java)

                    if (user?.username.toString() == campoUsernameAdmin.text.toString() && user?.password.toString() == loginPasswordAdmin.text.toString()) {
                        val intent = Intent(this@LoginAdmin, HomeAdminActivity::class.java)
                        startActivity(intent)
                    } else {
                        Snackbar.make(
                            findViewById(R.id.loginPasswordAdmin),
                            "Username o Password errati.",
                            Snackbar.LENGTH_LONG
                        ).show()
                    }
                }

                override fun onCancelled(dE: DatabaseError) {}
            })
        }
    }
}
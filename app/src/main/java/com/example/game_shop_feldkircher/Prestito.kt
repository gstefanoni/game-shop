package com.example.game_shop_feldkircher

class Prestito constructor(var id: String, var gioco: String, var userName: String, var dataInizio: String , var dataFine: String ) {

    constructor() : this("","","", "00000000", "00000000")

    override fun toString(): String {
        return id.toString()
    }

    fun set(nuovoPrestito: Prestito) {
        this.id = nuovoPrestito.id
        this.gioco = nuovoPrestito.gioco
        this.userName = nuovoPrestito.userName
        this.dataInizio = nuovoPrestito.dataInizio
        this.dataFine = nuovoPrestito.dataFine
    }

}

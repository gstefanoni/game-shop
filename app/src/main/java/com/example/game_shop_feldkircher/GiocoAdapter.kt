package com.example.game_shop_feldkircher


import android.content.Context
import android.graphics.Movie
import android.icu.number.NumberFormatter.with
import android.icu.number.NumberRangeFormatter.with
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Glide.with
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.squareup.*
import com.squareup.picasso.Picasso


class GiocoAdapter(var giochiList: List<Gioco>, var listener: OnItemClickListener) : RecyclerView.Adapter<GiocoAdapter.MyViewHolder>() {


    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener{
        var immagineGame: ImageView =
            view.findViewById(com.example.game_shop_feldkircher.R.id.immagineGame)

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            var position = adapterPosition
            if(position != RecyclerView.NO_POSITION) {
                listener.onItemClick(position,giochiList)
            }
        }

    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.game_list, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val storage = Firebase.storage("gs://gameshop-1614a.appspot.com")

        val storageReference = storage.getReference(giochiList[position].imgPath).downloadUrl.addOnSuccessListener {
            Picasso.get().load(it).fit().into(holder.immagineGame)
        }
    }

    override fun getItemCount(): Int {
        return giochiList.size
    }



    interface RecyclerViewClickListener{
        fun onClick(v: View, position: Int)
    }
}

interface OnItemClickListener { fun onItemClick(position: Int,giochiList: List<Gioco>) }
package com.example.game_shop_feldkircher

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.SearchView
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage

class RimuoviGiocoActivity : AppCompatActivity() {

    val database = Firebase.database("https://gameshop-1614a-default-rtdb.europe-west1.firebasedatabase.app/").getReference("Giochi")

    lateinit var listView: ListView
    var adapter: ListaGiochiAdapter? = null
    var arrayList : ArrayList<Gioco> = ArrayList()
    var arrayListFiltered: ArrayList<Gioco> = ArrayList()
    var childListener : ChildEventListener = getChildListener()
    var price: Boolean = true
    var date: Boolean = true
    var testRicerca: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rimuovi_gioco)

        listView = findViewById(R.id.giochi)

        adapter = ListaGiochiAdapter(this, arrayList)
        listView.adapter = adapter

        var searchView: SearchView = findViewById(R.id.ricerca)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                arrayListFiltered.removeAll(arrayListFiltered)
                testRicerca = query
                if(query == ""){
                    adapter = ListaGiochiAdapter(this@RimuoviGiocoActivity, arrayList)
                    listView.adapter = adapter
                    return false
                }

                arrayList.forEach {
                    if(query in it.nome)
                        arrayListFiltered.add(it)
                }
                adapter = ListaGiochiAdapter(this@RimuoviGiocoActivity, arrayListFiltered)
                listView.adapter = adapter
                return false
            }
            override fun onQueryTextChange(newText: String): Boolean {
                arrayListFiltered.removeAll(arrayListFiltered)
                testRicerca = newText
                if(newText == ""){
                    adapter = ListaGiochiAdapter(this@RimuoviGiocoActivity, arrayList)
                    listView.adapter = adapter
                    return false
                }
                arrayList.forEach {
                    if(newText in it.nome)
                        arrayListFiltered.add(it)
                }
                adapter = ListaGiochiAdapter(this@RimuoviGiocoActivity, arrayListFiltered)
                listView.adapter = adapter
                return false
            }
        })
    }

    fun tornaIndietro(view: View) {
        val intent = Intent(this@RimuoviGiocoActivity, HomeAdminActivity::class.java)
        startActivity(intent)
    }

    fun sortByDate(view: View){
        arrayListFiltered.removeAll(arrayListFiltered)
        var sortedList: List<Gioco> = arrayList.sortedWith(compareBy({ it.data.toInt() }))
        if(date) {
            sortedList.forEach {
                if(testRicerca in it.nome)
                arrayListFiltered.add(it)
            }
            date = false
        }else{
            sortedList.reversed().forEach {
                if(testRicerca in it.nome)
                    arrayListFiltered.add(it)
            }
            date = true
        }

        adapter = ListaGiochiAdapter(this@RimuoviGiocoActivity, arrayListFiltered)
        listView.adapter = adapter
        return
    }

    fun sortByPrice(view: View){
        arrayListFiltered.removeAll(arrayListFiltered)
        var sortedList: List<Gioco> = arrayList.sortedWith(compareBy({ it.prezzo.toDouble() }))
        if(price) {
            sortedList.forEach {
                if(testRicerca in it.nome)
                    arrayListFiltered.add(it)
            }
            price = false
        }else{
            sortedList.reversed().forEach {
                if(testRicerca in it.nome)
                    arrayListFiltered.add(it)
            }
            price = true
        }
        adapter = ListaGiochiAdapter(this@RimuoviGiocoActivity, arrayListFiltered)
        listView.adapter = adapter
        return
    }

    override fun onStart() {
        super.onStart()

        database!!.addChildEventListener(childListener)
    }

     internal fun getChildListener(): ChildEventListener {
        return object : ChildEventListener {

            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {

                val nuovoGioco = snapshot.getValue(Gioco::class.java)
                arrayList.add(nuovoGioco!!)
                adapter?.notifyDataSetChanged()

            }

            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {

                val nuovoGioco = snapshot.getValue(Gioco::class.java)
                val userKey = snapshot.key
                arrayList.find { e -> e.toString() == userKey }?.set(nuovoGioco!!)
                adapter?.notifyDataSetChanged()

            }

            override fun onChildRemoved(snapshot: DataSnapshot) {

                val userKey = snapshot.key
                val giocoRimosso = arrayList.find { e -> e.toString() == userKey }
                arrayList.remove(giocoRimosso)
                adapter?.notifyDataSetChanged()

            }

            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {

                Log.d("RimuoviGioco", "onChildMoved" + snapshot.key!!)

            }

            override fun onCancelled(error: DatabaseError) {

                Log.d("RimuoviGioco", "onCancelled" + error.toException())

            }

        }
    }

    override fun onStop() {
        super.onStop()

        database!!.removeEventListener(childListener)
    }
}

class ListaGiochiAdapter(private val context: Context, private val arrayList: ArrayList<Gioco>) : BaseAdapter() {
    private lateinit var nome: TextView
    private lateinit var prezzo: TextView
    private lateinit var categoria: TextView
    private lateinit var elimina: Button

    override fun getCount(): Int {
        return arrayList.size
    }
    override fun getItem(position: Int): Any {
        return position
    }
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var convertView = convertView
        convertView = LayoutInflater.from(context).inflate(R.layout.row_gioco, parent, false)
        nome = convertView.findViewById(R.id.nome)
        prezzo = convertView.findViewById(R.id.prezzo)
        categoria = convertView.findViewById(R.id.categoria)
        elimina = convertView.findViewById(R.id.elimina)
        nome.text = " " + arrayList[position].nome
        prezzo.text = arrayList[position].prezzo
        categoria.text = arrayList[position].categoria

        elimina.setOnClickListener(object : View.OnClickListener {
            override fun onClick(view: View?) {
                // Do some work here
                val database = Firebase.database("https://gameshop-1614a-default-rtdb.europe-west1.firebasedatabase.app/").getReference("Giochi")
                database.child(arrayList[position].id).removeValue()
                val storage = Firebase.storage("gs://gameshop-1614a.appspot.com").getReference(arrayList[position].imgPath)
                storage.delete()
            }
        })

        return convertView
    }
}